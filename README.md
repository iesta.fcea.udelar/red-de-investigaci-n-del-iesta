El presente trabajo corresponde a una primera visualización del cuerpo de investigadores del Instituto de Estadística de la Facultad de Ciencias Económicas y Administración de la Universidad de la República utilizando análisis de redes sociales. Se buscarán construir distintas redes de acuerdo con las características que se quieren investigar obteniéndose información que puede ser de utilidad para la dirección del Instituto. También se realiza una sistematización teórica acerca de la metodología a utilizar que puede ser de utilidad para futuras investigaciones en esta área. Los datos utilizados se encuentran en la carpeta "data" con el nombre "datos_red_iesta.xlsx". El script y las funciones utilizadas para el análisis de los mismos se encuentran en la carpeta "R" con el nombre "red_Iesta".




